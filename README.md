
# Aula 02

## Tarefa

Crie uma lista de todos atavés do javascript

### Requisitos

- Quando o usuario inserir um valor do tipo texto no campo `Nome Todo` e clicar em `Adicionar`, o todo deve aparecer imediatamente na lista acima.
- A lista deve ser persistida na api de `Window.localStorage`
- Ao carregar a pagina a lista atualizada deve ser carregada
- A Lista deve ser apresentada de forma ordenada 

Ex:
```html
1 Meu primeiro Todo
2 Meu segundo Todo
```
